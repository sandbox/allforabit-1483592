// $Id: sidebar.toolbar.js,v 1.1.2.9 2010/07/31 21:22:44 yhahn Exp $

Drupal.behaviors.sidebar = function(context) {
  $('#sidebar:not(.processed)').each(function() {
    var toolbar = $(this);
    toolbar.addClass('processed');

    // Set initial toolbar state.
    Drupal.sidebar.init(toolbar);

    // sidebar toggle.
    $('.sidebar-toggle', this).click(function() { Drupal.sidebar.toggle(toolbar); });

    // sidebar tabs.
    $('div.sidebar-tab', this).click(function() { Drupal.sidebar.tab(toolbar, $(this), true); });
  });
  $('div.sidebar-panes:not(.processed)').each(function() {
    var panes = $(this);
    panes.addClass('processed');

    $('h2.sidebar-pane-title a').click(function() {
      var target = $(this).attr('href').split('#')[1];
      var panes = $(this).parents('div.sidebar-panes')[0];
      $('.sidebar-pane-active', panes).removeClass('sidebar-pane-active');
      $('div.sidebar-pane.' + target, panes).addClass('sidebar-pane-active');
      $(this).addClass('sidebar-pane-active');
      return false;
    });
  });
};

/**
 * sidebar toolbar methods.
 */
Drupal.sidebar = {};

/**
 * Set the initial state of the toolbar.
 */
Drupal.sidebar.init = function (toolbar) {
  // Set expanded state.
  if (!$(document.body).hasClass('sidebar-ah')) {
    var expanded = this.getState('expanded');
    if (expanded == 1) {
      $(document.body).addClass('sidebar-expanded');
    }
  }

  // Set default tab state.
  var target = this.getState('activeTab');
  if (target) {
    if ($('div.sidebar-tab.'+target).size() > 0) {
      var tab = $('div.sidebar-tab.'+target);
      this.tab(toolbar, tab, false);
    }
  }

  // Add layout class to body.
  var classes = toolbar.attr('class').split(' ');
  if (classes[0] === 'nw' || classes[0] === 'ne' || classes[0] === 'se' || classes[0] === 'sw' ) {
    $(document.body).addClass('sidebar-'+classes[0]);
  }
  if (classes[1] === 'horizontal' || classes[1] === 'vertical') {
    $(document.body).addClass('sidebar-'+classes[1]);
  }
  if (classes[2] === 'df' || classes[2] === 'ah') {
    $(document.body).addClass('sidebar-'+classes[2]);
  }
};

/**
 * Set the active tab.
 */
Drupal.sidebar.tab = function(toolbar, tab, animate) {
  if (!tab.is('.sidebar-tab-active')) {
    var target = $('span', tab).attr('id').split('sidebar-tab-')[1];

    // Vertical
    // Use animations to make the vertical tab transition a bit smoother.
    if (toolbar.is('.vertical') && animate) {
      $('.sidebar-tab-active', toolbar).fadeOut('fast');
      $(tab).fadeOut('fast', function() {
        $('.sidebar-tab-active', toolbar).fadeIn('fast').removeClass('sidebar-tab-active');
        $(tab).slideDown('fast').addClass('sidebar-tab-active');
        Drupal.sidebar.setState('activeTab', target);
      });
    }
    // Horizontal
    // Tabs don't need animation assistance.
    else {
      $('div.sidebar-tab', toolbar).removeClass('sidebar-tab-active');
      $(tab, toolbar).addClass('sidebar-tab-active');
      Drupal.sidebar.setState('activeTab', target);
    }

    // Blocks
    $('div.sidebar-block.sidebar-active', toolbar).removeClass('sidebar-active');
    $('#block-'+target, toolbar).addClass('sidebar-active');
  }
  return false;
};

/**
 * Toggle the toolbar open or closed.
 */
Drupal.sidebar.toggle = function (toolbar) {
  if ($(document.body).is('.sidebar-expanded')) {
    if ($(toolbar).is('.vertical')) {
      $('div.sidebar-blocks', toolbar).animate({width:'0px'}, 'fast', function() { $(this).css('display', 'none'); });
      if ($(toolbar).is('.nw') || $(toolbar).is('sw')) {
        $(document.body).animate({marginLeft:'0px'}, 'fast', function() { $(this).toggleClass('sidebar-expanded'); });
      }
      else {
        $(document.body).animate({marginRight:'0px'}, 'fast', function() { $(this).toggleClass('sidebar-expanded'); });
      }
    }
    else {
      $('div.sidebar-blocks', toolbar).animate({height:'0px'}, 'fast');
      if ($(toolbar).is('.nw') || $(toolbar).is('ne')) {
        $(document.body).animate({marginTop:'0px'}, 'fast', function() { $(this).toggleClass('sidebar-expanded'); });
      }
      else {
        $(document.body).animate({marginBottom:'0px'}, 'fast', function() { $(this).toggleClass('sidebar-expanded'); });
      }
    }
    this.setState('expanded', 0);
  }
  else {
    if ($(toolbar).is('.vertical')) {
      $('div.sidebar-blocks', toolbar).animate({width:'150px'}, 'fast');
      if ($(toolbar).is('.nw') || $(toolbar).is('sw')) {
        $(document.body).animate({marginLeft:'150px'}, 'fast', function() { $(this).toggleClass('sidebar-expanded'); });
      }
      else {
        $(document.body).animate({marginRight:'150px'}, 'fast', function() { $(this).toggleClass('sidebar-expanded'); });
      }
    }
    else {
      $('div.sidebar-blocks', toolbar).animate({height:'150px'}, 'fast');
      if ($(toolbar).is('.nw') || $(toolbar).is('ne')) {
        $(document.body).animate({marginTop:'150px'}, 'fast', function() { $(this).toggleClass('sidebar-expanded'); });
      }
      else {
        $(document.body).animate({marginBottom:'150px'}, 'fast', function() { $(this).toggleClass('sidebar-expanded'); });
      }
    }
    if ($(document.body).hasClass('sidebar-ah')) {
      this.setState('expanded', 0);
    }
    else {
      this.setState('expanded', 1);
    }
  }
};

/**
 * Get the value of a cookie variable.
 */
Drupal.sidebar.getState = function(key) {
  if (!Drupal.sidebar.state) {
    Drupal.sidebar.state = {};
    var cookie = $.cookie('DrupalSidebar');
    var query = cookie ? cookie.split('&') : [];
    if (query) {
      for (var i in query) {
        // Extra check to avoid js errors in Chrome, IE and Safari when
        // combined with JS like twitter's widget.js.
        // See http://drupal.org/node/798764.
        if (typeof(query[i]) == 'string' && query[i].indexOf('=') != -1) {
          var values = query[i].split('=');
          if (values.length === 2) {
            Drupal.sidebar.state[values[0]] = values[1];
          }
        }
      }
    }
  }
  return Drupal.sidebar.state[key] ? Drupal.sidebar.state[key] : false;
};

/**
 * Set the value of a cookie variable.
 */
Drupal.sidebar.setState = function(key, value) {
  var existing = Drupal.sidebar.getState(key);
  if (existing != value) {
    Drupal.sidebar.state[key] = value;
    var query = [];
    for (var i in Drupal.sidebar.state) {
      query.push(i + '=' + Drupal.sidebar.state[i]);
    }
    $.cookie('DrupalSidebar', query.join('&'), {expires: 7, path: '/'});
  }
};
