<div id='sidebar' class='<?php print $position ?> <?php print $layout ?> <?php print $behavior ?>'>
  <span class='sidebar-toggle'><?php print t('sidebar') ?></span>
  <div class='sidebar-blocks sidebar-blocks-<?php print count($blocks) ?>'>
    <?php foreach ($blocks as $bid => $block): ?>
      <div class='sidebar-block <?php if (isset($block->class)) print $block->class ?>' id='block-<?php print $bid ?>'>
        <div class='block-content clear-block'><?php print $block->content ?></div>
      </div>
    <?php endforeach; ?>
  </div>

</div>
