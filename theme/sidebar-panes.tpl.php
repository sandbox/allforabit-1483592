<div class='sidebar-panes clear-block '>
  <?php if ($layout == 'vertical'): ?>

    <?php foreach ($panels as $key => $panel): ?>
      <div class='sidebar-pane <?php if (!isset($first)) print 'sidebar-pane-active' ?> sidebar-pane-<?php print $key ?>'>
        <h2 class='sidebar-pane-title'><?php print $labels[$key] ?></h2>
        <div class='sidebar-pane-content clear-block'><?php print $panel ?></div>
      </div>
      <?php $first = TRUE ?>
    <?php endforeach; ?>

  <?php print drupal_render($others) ?>
</div>
